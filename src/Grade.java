import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

public class Grade {
    public static void main(String[] args) throws InterruptedException{
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver74\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        String appUrl = "http://portal.aait.edu.et";
        driver.get(appUrl);

        String gradeUrl = "https://portal.aait.edu.et/Grade/GradeReport";


        driver.findElement(By.xpath("//*[@id=\"UserName\"]")).sendKeys(Credentials.portalUsername);
        driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys(Credentials.portalPassword);
        driver.findElement(By.xpath("//*[@id=\"home\"]/div[2]/div[2]/form/div[4]/div/button")).click();

        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);

        driver.navigate().to(gradeUrl);
        String gradeOutput = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div")).getText();

        try{
            PrintWriter writer = new PrintWriter("gradefile.txt", "UTF-8");
            writer.println("Grade");
            writer.println();
            writer.println(gradeOutput);
            writer.close();
        }catch(Exception e){
            e.printStackTrace();
        }

        Thread.sleep(5000000);

        driver.close();
    }
}
