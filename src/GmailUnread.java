import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GmailUnread {
    public static void main(String[] args) throws InterruptedException{
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver74\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 20);

        String appUrl = "http://gmail.com";
        driver.get(appUrl);

        driver.findElement(By.xpath(".//*[@id=\"identifierId\"]")).sendKeys(Credentials.gmail);
        driver.findElement(By.xpath(".//*[@id=\"identifierNext\"]/span/span")).click();
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);

        WebElement passwordTextField = driver.findElement(By.xpath(".//*[@id=\"password\"]/div[1]/div/div[1]/input"));
        wait.until(ExpectedConditions.elementToBeClickable(passwordTextField));
        passwordTextField.sendKeys(Credentials.gmailPassword);

        driver.findElement(By.xpath(".//*[@id=\"passwordNext\"]/span/span")).click();
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);

        List<WebElement> rows = driver.findElements(By.cssSelector(".zA.zE"));
        System.out.println("row" + rows.get(0).getText());

        try{
            PrintWriter writer = new PrintWriter("unreadMail.txt", "UTF-8");
            writer.println("Unread Mail");
            writer.println("____________________________________________________________\n");
            writer.println();
            for (WebElement row : rows) {
                writer.println(row.getText());
                writer.println();
            }
            writer.close();
        }catch(Exception e){
            e.printStackTrace();
        }

        Thread.sleep(5000000);

        driver.close();
    }
}
